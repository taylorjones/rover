# Mini-Project 2: Robot Motion
Kenny Bradley

Brady Houston

Taylor Jones

Rachel Wall

## Execution Instructions
### Installation Instructions
1. Ensure you are in the src directory of your catkin_ws
2. Put the rover.rosinstall in this src folder
3. Run: rosinstall . rover.rosinstall
4. Run: rosdep install --from-paths . --ignore-src
5. Then go to the root of your catkin workspace and run: catkin_make
### Running Instructions
1. Source your workspace: source devel/setup.bash
2. Launch RVIZ and Teleop: roslaunch rover kinematics.launch
3. This will launch RVIZ. Control the robot within the same terminal it was started in

## Moving Vehicle
The robot is controlled using the keyboard. Make sure the terminal is selected while trying to move.
The 'w'/'x' keys add forward/backward linear accelerations, respectively.  
The 'a'/'d' keys add left/right rotational accelerations. 
The 's' key zeroes out the accelerations. 
The terminal will show what the current values are set to.

## Updated Dynamics
### Model & Equations
We used the forward drive kinematics built for mecanum wheels for this project. These kinematics are found at this url: http://robotsforroboticists.com/drive-kinematics/ .
The equations for each wheel are detailed below:

wheel_front_left = (1/WHEEL_RADIUS) * (linear.x – linear.y – (WHEEL_SEPARATION_WIDTH + WHEEL_SEPARATION_LENGTH)*angular.z);

wheel_front_right = (1/WHEEL_RADIUS) * (linear.x + linear.y + (WHEEL_SEPARATION_WIDTH + WHEEL_SEPARATION_LENGTH)*angular.z);

wheel_rear_left = (1/WHEEL_RADIUS) * (linear.x + linear.y – (WHEEL_SEPARATION_WIDTH + WHEEL_SEPARATION_LENGTH)*angular.z);

wheel_rear_right = (1/WHEEL_RADIUS) * (linear.x – linear.y + (WHEEL_SEPARATION_WIDTH + WHEEL_SEPARATION_LENGTH)*angular.z);

### Justification
This kinematic model was chosen because the physical rover we are designing around has mecanum wheels. Mecanum wheels are unique; therefore, we want to use kinematics specific to them. 

### Code Implementation
First , two new packages were made: one for the kinematics and one for the control (this was the controls detailed above in the rover_teleop package). 

Within the kinematics package, we built a file for the robot called rover.cpp. 
rover.cpp is based heavily on the provided unicycle.cpp file. This section will describe the changes from the unicycle.cpp file to show how we changed the object for our future kinematics.
Building the rover.cpp involved building a Rover object and adding the linear and angular acceleration variables. 
Next, instead of subscribing to cmd_vel (velocity), we subscribe to cmd_accel (acceleration). With this change we built a callback for accel. instead of velocity. 
In the callback, we set the respective accelerations of the robot from the accel. message (this is how we manipulate accelerations instead of velocities). 
Since we have x and y  velocity, we updated the odometry in terms of x and y.
This ends the changes for the rover.cpp file. With this file, we can now build a new file for modifying the rover's motion (kinematics).

Next, we built a file called rover_joints.cpp. This file modifies the robot's joints so that it can move using new kinematics.
First, we built a rover_joints object and read in the joints of the four wheels of our rover. Then, we initialize variables to keep track of the current speed, last position, and last velocity for all four wheels.
Then, we subscribed to the odometry topic (to get orientation), and started a publisher for the joint states. We also wrote an updater (update()) that will publish the current state of the joints.
Next we built the update Joint function. This function updates the linear and rotational velocities of the joints. 
Updtaing these velocities is done by setting the linear velocities of each wheel to the wheel_speed_cmd variables being read in and by setting the rotational velocities to the linear velocities divided by the radius of the wheels.
Also, we update the last positions and velocities to the current positions and velocities for the next time step.
Lastly, we built a function called odometryCallback (runs when a new odemetry message arrives). Here, we set our velocities to the read in twist velocities. 
Finally, we set the respective wheel_speed_cmd variables to the new kinematics equations above (seen on lines 130-133 of the code).
With this, we have implemented our new kinematics. 

Lastly, we built a kinematics.launch file to run the project. It is here where we remap from the cmd_vel topic to the cmd_accel topic for the teleop so that we can control with accelerations.
## Moving visualization
No changes were made to our rover from Mini-project 1 in terms of it's looks (the URDF was not modified). 


# MINI PROJECT 1
Kenny Bradley

Brady Houston

Taylor Jones

Rachel Wall

## Execution Instructions
### Installation Instructions
1. Ensure you are in the src directory of your catkin_ws
2. Put the rover.rosinstall in this src folder
3. Run: rosinstall . rover.rosinstall
4. Run: rosdep install --from-paths . --ignore-src
5. Then go to the root of your catkin workspace and run: catkin_make
### Running Instructions
1. Source your workspace: source devel/setup.bash
2. Launch RVIZ and Gazebo: roslaunch rover launch/sim.launch
3. Control the robot in Gazebo with the pop up controls
## Component Details
### Components Description
Our group made all the components for this robot. The robot has seven types of components: base, battery, ESCs, Raspberry Pi, Arduino Uno, wheels, and motors.
The root is the base platform of the robot, which is shown in black. The robot battery is indicated by a blue box on top of the base at the rear of the robot. The ESCs are indicated by a grey box on top of the base near the middle. The Raspberry Pi is indicated by the green component elevated above the robot's base near the front. The Arduino Uno is indicated by the blue component on top of the base, near the front. The four wheels (shown as silver cylinders near each corner of the base) are each attached to their own motor (silver) underneath the base platform’s corners.
### Fixed Links and their Joints
#### Description
The first link we made within the robot visualization that all other links are based on is the base_link. This link is the base platform for the rover (line 12 of the URDF). The following Fixed Joints table shows the fixed joints, links, and code locations: 

Fixed Joints: 

|Joint  (Code Line)|Parent  Link (Code Line)|Child Link (Code Line)|
|:--------------------:|:---------------:|:-------------------:|
|battery_joint (51)     |base_link (12)   | battery_link (33)   |
|esc_joint (79)         |base_link(12)    |esc_link(61)         |           |raspberryPi_joint (107)|base_link(12)   |raspberryPi_link (89)|           |arduinoUno_joint (135) |base_link(12)    |arduinoUno_link (117)|
|*_motor_joint (160)    |base_link(12)    |*=_motor (142)       |

*= left/right_back/front
#### Transforms
The transform table details the robots’ transforms: 

Transform Table: 

| From Parent Link | To Child Link | Translation| Rotation |
|:----------------:|:-------------:|:----------:|:-------:|
| base_link | raspberryPi_link | (-0.04 -0.01 0.061) | (0 0 0) |
| base_link | arduinoUno_link | (${pi/2} 0 ${pi/2}) | (-0.025 0 0) |
| base_link | esc_link | (0 0 0.011) | (0 0 0) |
| base_link | battery_link | (0 -0.1 0.002) | (0 0 0) |
| base_link | left_front_motor | (-0.064 0.07 -0.019) | (0 0 ${pi}) |
| base_link | right_front_motor | (0.064 0.07 -0.019) | (0 0 ${pi}) |
| base_link | left_back_motor | (-0.064 -0.1 -0.019) | (0 0 0) |
| base_link | right_back_motor | (0.064 -0.1 -0.019) | (0 0 0) |
| *_motor | *_wheel | (0.00972 0 0) | (0 0 0) |

*= left/right_back/front


### Moving Links and their Joints
#### Description
The robot has four wheels that rotate. These wheel links (i.e. left_back_wheel) are attached to their respective wheel_motor links (i.e. left_back_motor). These wheels attach to their motors through continuous joints (i.e. left_back_wheel_joint) to rotate 360 degrees.

Continuous Joint: 

|Joint  (Code Line)|Parent  Link (Code Line)|Child Link (Code Line)|
|:---------------:|:----------------------:|:--------------------:|
|left/right_back/front_wheel_joint (160)|left/right_back/front_motor (142)|left/right_back/front_wheel(213)|